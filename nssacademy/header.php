<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fonts/font.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=1.1" type="text/css">
        <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
       
        <?php wp_head(); ?>
        <!-- Crazy Egg Tracking -->
        <script type="text/javascript">
            setTimeout(function(){var a=document.createElement("script");
                var b=document.getElementsByTagName("script")[0];
                a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0041/1960.js?"+Math.floor(new Date().getTime()/3600000);
                a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
        </script>
        <!-- Hotjar Tracking Code for https://www.nssacademy.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:544831,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    </head>

    <body <?php body_class(); ?>>
        <header class="site_header <?php if(is_front_page()):?>home_header<?php endif;?>"> 

            <!--Top-Strip-->
            <div class="top-strip">
                <div class="container">
                    <div class="pull-right">
                        <ul class="top-details">
                            <!-- <li><a href="#">SA Blog</a></li> -->
                            <li><a href="<?php echo get_permalink(13);?>"><i class="fa fa-map-marker"></i> Find Us</a></li>
                            <li><a href="tel:<?php echo ot_get_option("phone_number"); ?>"><?php echo ot_get_option("phone_number"); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--Top-Strip-end-->

            <div class="menu-area">
                <div class="container">
                    <nav class="navbar navbar-inverse">
                        <div class="navbar-header">
                            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> <span class="menu-ico-txt">Menu</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo"><img src="<?php echo $header_logo = ot_get_option("logo"); ?>" alt="logo"></a> </div>
                        <div class="collapse navbar-collapse js-navbar-collapse navbar-right">
                            <?php wp_nav_menu(array('container' => '', 'menu' => 'Header Menu', 'menu_class' => 'nav navbar-nav main-menu')); ?>

                        </div>
                    </nav>
                </div>
            </div>
        </header>

        <!-- header end here --> 