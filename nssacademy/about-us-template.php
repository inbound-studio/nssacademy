<?php
/** 
* Template Name: About Page
*/
get_header();
?>

<!-- page title section -->
<div class="page_title">
  <div class="container">
    <h1><?php the_title();?> </h1>
  </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
  <div class="container">
     <?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
  </div>
</div>
<div class="main-content">
  <div class="container">
      <?php while ( have_posts() ) : the_post(); ?>
    <div class="students">
     <?php the_content();?> 
    </div>
<?php endwhile; wp_reset_postdata();?>
<div class="program-section">
    <h3>About The Program </h3>
    <p>Summer Academy hosts over 1,250 students and 60 classes every summer. Students come from across the north metro area, and we want to make your first year at Summer Academy the best possible experience. </p>
    <div class="program-tabs">
     

<div id="parentHorizontalTab">
            <ul class="resp-tabs-list hor_1">
                 <?php
            if ( $facility = get_field('facility') ):
            foreach( $facility as $facilitys ):
            ?>
              <li><?php echo $facilitys['title'];?></li>
             <?php endforeach; endif; ?>
            </ul>
            <div class="resp-tabs-container hor_1 tab-content">
                <?php
            if ( $facilitydata = get_field('facility') ):
            foreach( $facilitydata as $facilitydatas ):
            ?>
        <div>
          <article>
            <h4><?php echo $facilitydatas['title'];?></h4>
           <?php echo $facilitydatas['facility_description'];?>
          </article>
        </div>
         <?php endforeach; endif; ?>
      

            </div>
        </div>



</div>

</div>
    </div>
  </div>
</div>
<?php get_template_part( 'inc/find', 'more' ); ?>
<?php get_template_part( 'inc/parent', 'testimonial' ); ?>
<?php get_template_part( 'inc/vision', 'logo' ); ?>
<?php get_template_part( 'inc/twitter', 'footer' ); ?>
<?php get_footer();?>