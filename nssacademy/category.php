<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); 
$cat = get_query_var('cat');
$yourcat = get_category ($cat); 
$cid = get_query_var('cat');
 ?>

 <!-- page title section -->
<div class="page_title">
  <div class="container">
    <h1<?php echo $yourcat->name?></h1>
  </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
  	<div class="container">
     <?php if ( function_exists('yoast_breadcrumb') ) 
			{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
  	</div>
</div>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="blog-area">
        <div class="col-sm-8 blog-main">
        	 <?php wp_reset_query(); ?>
                <?php   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;   
                     $catarg = array('post_type'=>'post','category_name'=>$yourcat->slug,'paged' => $paged,'posts_per_page' => 4);            

                     query_posts($catarg);
                     while ( have_posts() ) : the_post();
                        $recent_id=get_the_id();
                        $recent_post_image =wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                         //wp_get_attachment_url( get_post_thumbnail_id($recent_id) );                                    
              ?>
          <div class="blog-post">
          	<?php if ( has_post_thumbnail() ):?>
	            <div class="blog-pic"><a href="<?php the_permalink();?>">
	             	<img src="<?php echo $recent_post_image[0];?>" alt="<?php echo get_the_title();?>"></a>
	            </div>
         	<?php endif;?>
            <h2 class="blog-post-title"><a href="<?php the_permalink();?>"><?php echo get_the_title();?></a></h2>
            <p class="blog-post-meta"><span>By: <?php the_author();?></span> <span><?php echo $date=get_the_date();?></span></p>
             <?php $blog_content= get_the_content();
                  $blogstrip_content=strip_tags($blog_content); 
                  echo "<p>".substr($blogstrip_content,0,400)."...</p>";
            ?>   
            <a href="<?php the_permalink();?>" class="read">read more</a> </div>
            <?php endwhile;?>

          <!-- pagination-->
         <?php  wp_pagenavi(); ?>
         <!-- end pagination -->
           <?php wp_reset_query();?>  		  
          <!-- /.blog-post -->
         
        </div>
        <!-- /.blog-main -->         
        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
        	 <?php dynamic_sidebar( 'sidebar-2' ); ?>          
        </div>
        <!-- /.blog-sidebar --> 
      </div>
    </div>
    <!-- /.row --> 
    
  </div>
</div>
<?php get_footer();?>