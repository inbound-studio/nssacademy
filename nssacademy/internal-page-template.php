<?php
/** 
* Template Name: Internal Page
*/
get_header();
?>

<!-- page title section -->
<div class="page_title">
  <div class="container">
    <h1><?php the_title();?> </h1>
  </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
  <div class="container">
     <?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
  </div>
</div>
<div class="main-content">
  <div class="container">
      <?php while ( have_posts() ) : the_post(); ?>
    <div class="students">
     <?php the_content();?> 
    </div>
<?php endwhile; wp_reset_postdata();?>

</div>
    </div>
  </div>
</div>
<?php // get_template_part( 'inc/find', 'more' ); ?>
<?php // get_template_part( 'inc/parent', 'testimonial' ); ?>
<?php // get_template_part( 'inc/vision', 'logo' ); ?>
<?php // get_template_part( 'inc/twitter', 'footer' ); ?>
<?php get_footer();?>