<?php
/**
 * Template Name: FAQ Page
 */
get_header();
?>

<!-- page title section -->
<div class="page_title">
    <div class="container">
        <h1><?php the_title();?></h1>
    </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
    <div class="container">
         <?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
    </div>
</div>
<div class="main-content">
    <div class="container">
        <h2><?php the_title();?></h2>
        <div class="faq_section">
            <?php
            $i = 1;
            $faq_categories = get_terms('faq-category');
            $count = sizeof($faq_categories);
            for ($j = 0; $j < $count; $j++) {
                ?>

                <h3><?php echo $faq_categories[$j]->name; ?></h3>

                <div class="panel-group" id="accordion<?php echo $j; ?>">
                    <?php
                    $args = array(
                        'post_type' => 'faqs',
                        'showposts' => '-1',
                        'order' => 'desc',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'faq-category',
                                'field' => 'id',
                                'terms' => $faq_categories[$j]->term_id
                            )),
                    );

                    $faq = get_posts($args);
                    foreach ($faq as $post): setup_postdata($post);
                        $faq_id = get_the_id();
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion<?php echo $j; ?>" href="#one<?php echo $i; ?>"><?php the_title(); ?></a> </h4>
                            </div>
                                <?php if ($i == 1) { ?>
                                <div id="one<?php echo $i; ?>" class="panel-collapse collapse in">
                                    <?php } else { ?>
                                    <div id="one<?php echo $i; ?>" class="panel-collapse collapse ">
                                        <?php } ?>
                                    <div class="panel-body">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php $i++;
                    endforeach; ?>


                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
   <?php get_template_part( 'inc/find', 'more' ); ?>
<?php get_footer(); ?>