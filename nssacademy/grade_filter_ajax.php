<?php
include '../../../wp-load.php';
switch ($_REQUEST['action']) {
    case 'show_class':
        $pid = $_REQUEST['metakey'];
        if ($pid == "all"):
            $args = array(
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'class',
                'post_status' => 'publish',
                'suppress_filters' => true);

        else:
            $args = array(
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'class',
                'meta_query' => array(
                    array(
                        'key' => 'grdaes',
                        'value' => $pid,
                        'compare' => 'like'
                    )
                ),
                'post_status' => 'publish',
                'suppress_filters' => true);
        endif;
       
        $myposts = get_posts($args);
        if ($myposts):
            foreach ($myposts as $post) : setup_postdata($post);
                $feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                ?>
                <div class="col-sm-6 col-md-4">
                    <div class="class">
                        <figure><a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $feat_image; ?>&h=230&w=360" alt=""></a></figure>
                        <article> <span class="grade_rate">Class # <?php echo $post->menu_order; ?> &nbsp; <img src="<?php bloginfo('template_url'); ?>/images/grade-icon.png" alt="">Grades: <?php
                           $field_grade = get_post_meta($post->ID, "grdaes", true);
                                $i = 1;
                                foreach ($field_grade as $field_grade_val) {
                                    $count = count($field_grade);
                                    ?>
                                    <?php
                                    if ($i === $count) {
                                        echo str_replace("select_", " ", $field_grade_val);
                                    } else {
                                        echo str_replace("select_", " ", $field_grade_val) . "-";
                                    }
                                    ?>
                            <?php $i++;} ?></span>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>" class="button default">Full details </a> </article>
                    </div>
                </div>

                <?php
            endforeach;
            wp_reset_postdata();
        else:
            ?> 
            <h1 style="text-align: center;">No Class Found.</h1>
        <?php
        endif;

        die();
        break;
}
?>