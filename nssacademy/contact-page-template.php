<?php
/** 
* Template Name: Contact Page
*/
get_header();
?>
 <?php while ( have_posts() ) : the_post(); ?>
<!-- page title section -->
<div class="page_title">
  <div class="container">
    <h1><?php the_title();?></h1>
  </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
  <div class="container">
  <?php if ( function_exists('yoast_breadcrumb') ) 
{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
  </div>
</div>


<div class="main-content">
  <div class="container">
  <div class="contact-details">
  <h2>Get in Touch</h2>
  <div class="row">
 <?php the_content();?>
  </div>
  </div>
  
  
 <div class="contact_map"> 
  <h3>Location During Summer Academy</h3>
       <!--  <div class="map">
   <img src="<?php echo get_template_directory_uri(); ?>/images/map.jpg" alt="">
   <?php the_field("address");?> -->
  <div class="google-maps">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2818.480959735057!2d-93.24255068454929!3d45.05575367909824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b32e45568821ed%3A0xfd3c9976fbc1aaeb!2sColumbia+Heights+High+School!5e0!3m2!1sen!2sus!4v1455903425374" width="1170" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
  
  </div>
 </div> 
</div>
 <?php endwhile ; ?>
<?php get_template_part( 'inc/vision', 'logo' ); ?>
<?php get_footer(); ?>