<div class="logo-listing">
     <?php
                        $id = 28;
                        $post = get_post($id);
                        $content = apply_filters('the_content', $post->post_content);
                        ?>

                       
    <div class="container">
        <h2><?php echo get_the_title($id);?></h2>
        <div class="big-txt">
            <?php echo $content; wp_reset_postdata(); ?>
        </div>
        <ul class="logo-list">
            <?php
            if ( $logos = get_field('logo',28) ):
            foreach( $logos as $logo ):
            ?>
            <li><img src="<?php echo $logo['image']; ?>" alt=""></li>
              <?php endforeach; endif; ?>
        </ul>
    </div>
</div>