<?php
$tweet = do_shortcode('[statictweets skin="default" resource="usertimeline" user="" list="" query="" count="1" retweets="on" replies="on" show="username,screenname,avatar,time,actions,media"/]');
echo '<div style="display:none">' . $tweet . '</div>';
?>
<div class="twitter">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 border-twitter"><i class="fa fa-twitter"></i></div>
            <div class="col-xs-8 col-md-6">
                <div class="twitter_slider">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('.atf-tweet-text').each(function () {
            var txt = jQuery(this).text();
            jQuery('.twitter_slider').append('<div><div class="big-txt"> <p>' + txt + '</p></div><span class="twt_time"></span> </div>');
        })

        var i = 0;
        jQuery('.atf-tweet-time').each(function () {
            var tm = jQuery(this).text();
            jQuery('.twt_time:eq(' + i + ')').text(tm);
            i++;
        })

    })
</script>