<div class="parents_section">
    <div class="container">
        <h2>What Parents Have To Say</h2>
        <div class="row">
            <div class="parent_comments">
                <?php
                $args = array(
                'posts_per_page' => -1,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'testimonial',
                'post_status' => 'publish',
                'suppress_filters' => true);
                $myposts = get_posts($args);
                foreach ($myposts as $post) : setup_postdata($post);
                ?>
                <div>
                    <div class="block">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>