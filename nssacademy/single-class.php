<?php
get_header();
?>
<!-- page title section -->
<div class="page_title">
    <div class="container">
        <h1>Classes</h1>
    </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
    <div class="container">
         <ul class="breadcrumb">
      <li><a href="<?php echo home_url();?>">Home </a>
          <a href="<?php echo get_permalink(5);?>"> Classes</a>
          <span class="breadcrumb_last"> <?php the_title(); ?></span></li>
    </ul>
      
    </div>
</div>
<?php
while (have_posts()) : the_post();
    $feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
    ?>
    <div class="main-content">
        <div class="container">
            <h2><?php the_title(); ?></h2>
            <span class="grade_rate">
                           Class # <?php echo $post->menu_order; ?> &nbsp;
                        <img src="<?php bloginfo('template_url'); ?>/images/grade-icon.png" alt="">Grades: <?php
                           $field_grade = get_post_meta($post->ID, "grdaes", true);
                                $i = 1;
                                foreach ($field_grade as $field_grade_val) {
                                    $count = count($field_grade);
                                    ?>
                                    <?php
                                    if ($i === $count) {
                                        echo str_replace("select_", " ", $field_grade_val);
                                    } else {
                                        echo str_replace("select_", " ", $field_grade_val) . "-";
                                    }
                                    ?>
                            <?php $i++;} ?>
                            </span>
            <div class="stage-pic pull-right"><img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $feat_image; ?>&h=313&w=486" alt=""></div>
            <h3>Class Overview</h3>
            <div class="clearfix"></div>
            <?php the_content(); ?>

            <?php
        endwhile;
        wp_reset_postdata();
        ?>
        <?php
        $teachers = get_field('teacher');
        if ($teachers):
            ?>
            <h3>Teachers</h3>
            <?php foreach ($teachers as $post): ?>
                <?php
                setup_postdata($post);
                $teacher_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                ?>
                <div class="teachers">
                    <h5> <?php the_title(); ?></h5>
                    <div class="pic pull-right"><img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $teacher_image; ?>&h=144&w=144" alt=""></div>
                    <?php the_content(); ?>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
            ?> 
        <?php endif; ?>
        <?php
        $video_link = get_field('video_link');
        if ($video_link):
            ?>
            <h3>Videos</h3>

            <div class="video_block"> <?php the_field("video_link"); ?> </div>
        <?php endif; ?>
        <?php
        $additional_info = get_field('additional_info');
        if ($additional_info):
            ?>
            <h3>Additional Info</h3>
            <?php the_field("additional_info"); ?>
        <?php endif; ?>

        <div class="find-more">
            <div class="container">
                <h4>interested in this course?</h4>
                <p>Great! Then add it to your list of favorites and keep browsing. Then when ready - apply online!<br>
                    Remember you can apply for up to 5 courses but you’ll only be placed in one course. </p>
                <a href="/how-to-register/" class="button secondary_btn register_btn">HOW TO REGISTER</a> <a href="/register/" class="button secondary_btn apply-online-btn" target="_blank">REGISTER ONLINE</a> </div>
        </div>

    </div>
</div>

<?php get_footer(); ?>