<?php

get_header(); 
$id=get_the_id();
$post_categories = wp_get_post_categories($id ); 
?>
<!-- page title section -->
<div class="page_title">
  <div class="container">
    <h1><?php the_title();?> </h1>
  </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
  	<div class="container">
     <?php if ( function_exists('yoast_breadcrumb') ) 
			{yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>','</li></ul>');} ?>
  	</div>
</div>
<div class="main-content">
  <div class="container">
    <div class="row">
      <div class="blog-area">
        <div class="col-sm-9 blog-main blog-details center-wrap">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
				$postid=get_the_id();
          		$recent_post_image =wp_get_attachment_image_src( get_post_thumbnail_id( $postid ), 'full' );
			?>

			<div class="blog-post">
	          	<?php if ( has_post_thumbnail() ):?>
		            <div class="blog-pic">
		             	<img src="<?php echo $recent_post_image[0];?>" alt="<?php echo get_the_title();?>">
		            </div>
	         	<?php endif;?>
	            <h2 class="blog-post-title"><a href="<?php the_permalink();?>"><?php echo get_the_title();?></a></h2>
	            <p class="blog-post-meta"><span>Apply By: <?php echo (types_render_field( 'application-due-by', array() ));?></span> <span>Published: <?php echo $date=get_the_date();?></span></p>
	             <?php the_content();
	            ?> 
				
				<a class="button secondary_btn apply-online-btn" href="<?php echo (types_render_field( 'application-url', array('output' => 'raw') ));?>" target="_blank" rel="noopener">Apply Now</a>
				<p></p>
				
	           <?php echo (types_render_field( 'field-slug', array() ));?>
           	</div>
            <?php endwhile;?>
			<div class="share"><!-- <img src="<?php bloginfo('template_url');?>/images/social.jpg" alt="social"> -->
              	<?php if (function_exists('synved_social_share_markup')) echo synved_social_share_markup();?>
            </div>         
            <div id="comment">
                <?php if (comments_open()):
                    comments_template();  
                 endif;?>
            </div>
         <!-- end pagination -->
           <?php wp_reset_query();?>  		  
          <!-- /.blog-post -->         
        </div>        
       </div>
	</div>  
</div>

<?php get_footer();
