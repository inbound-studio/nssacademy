<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-6 col-md-3">

                <?php dynamic_sidebar("sidebar-3");?>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-3">

               <?php dynamic_sidebar("sidebar-4");?>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-3 text-box">

                <?php dynamic_sidebar("sidebar-5");?>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-3">
                <?php dynamic_sidebar("sidebar-6");?>
            </div>
        </div>
    </div>
    <div class="copy-right">
        <div class="container">
           <?php echo ot_get_option("copyright_message"); ?>
            <div class="social"> <a href="<?php echo ot_get_option("twitter_link"); ?>"><i class="fa fa-twitter"></i></a> <a href="<?php echo ot_get_option("facebook_link"); ?>"><i class="fa fa-facebook-square"></i></a> </div>
        </div>
    </div>
    <div class="scroll_top"><img src="<?php echo get_template_directory_uri(); ?>/images/scroll-top-btn.png" alt=""></div>
</footer>
<?php if (is_front_page()): ?>
<script>
    jQuery(function(){
        jQuery('#future_date').countdowntimer({
            dateAndTime : "2016/06/14 08:00:00",
            size : "lg",
            regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})",
regexpReplaceWith: "<div class='number-counter'>$1<sup class='displayformat'>days</sup> </div><div class='number-counter'> $2<sup class='displayformat'>hours</sup></div> <div class='number-counter'> $3<sup class='displayformat'>minutes</sup> </div><div class='number-counter'> $4<sup class='displayformat'>seconds</sup></div>"
        });
    });
</script>
<?php endif; ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.countdownTimer.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<?php wp_footer(); ?>
</body>
</html>
