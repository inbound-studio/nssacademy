<?php
/**
* Template Name: Home Page
*/
get_header();
?>
<?php

	$the_query = new WP_Query( 'page_id=44' );

	$the_query->have_posts();

	$the_query->the_post();

	$title=get_the_title();

	$image = wp_get_attachment_url(get_post_thumbnail_id(44));

	$content=get_the_content();
?>
<!-- banner section -->
<div class="banner"> <img src="<?php echo $image; ?>" alt="">
  <div class="banner-caption">
    <div class="container">
      <div class="pull-left">
       <?php echo $content;?>
        <a href="<?php echo get_permalink(5);?>" class="button primary hero"><img src="<?php echo get_template_directory_uri(); ?>/images/more-detail-arrow.png">VIEW CLASSES</a>
        <a href="<?php echo get_permalink(493);?>" class="button primary2 hero2"><img src="<?php echo get_template_directory_uri(); ?>/images/register--btn-icon.png">DOWNLOAD FORMS</a> </div>
    </div>
  </div>
</div>
<div class="home_section2">
  <div class="features">
    <div class="container">
      <div class="box_1"> <a href="<?php echo get_permalink(5);?>"><img src="<?php echo get_field("box_1_image",44)['url'];?>" alt="<?php echo get_field("box_1_image",44)['alt'];?>"> <strong><?php echo get_field("box_1_title",44);?></strong>
        <p><?php echo get_field("box_1_text",44);?></p>
        </a> </div>
      <div class="box_2"> <a href="<?php echo get_permalink(368);?>"><img src="<?php echo get_field("box_2_image",44)['url'];?>" alt="<?php echo get_field("box_2_image",44)['alt'];?>"> <strong><?php echo get_field("box_2_title",44);?></strong>
        <p><?php echo get_field("box_2_text",44);?></p>
        </a> </div>
      <div class="box_3"> <a href="<?php echo get_permalink(133);?>"><img src="<?php echo get_field("box_3_image",44)['url'];?>" alt="<?php echo get_field("box_3_image",44)['alt'];?>"> <strong><?php echo get_field("box_3_title",44);?></strong>
        <p><?php echo get_field("box_3_text",44);?></p>
        </a> </div>
        
              <div class="box_4"> <a href="<?php echo get_field("box_4_url",44);?>"><img src="<?php echo get_field("box_4_image",44)['url'];?>" alt="<?php echo get_field("box_4_image",44)['alt'];?>"> <strong><?php echo get_field("box_4_title",44);?></strong>
        <p><?php echo get_field("box_4_text",44);?></p>
        </a> </div>
        
        <!-- **** Blog Message **** -->
<!--
        <div class="box_4_blog" style="background: green;">
        <a href="http://blog.nssacademy.com/sa-blog"><img src="../wp-content/uploads/2016/02/box4-hdr.png">
        <strong>SA BLOG</strong>
        <p>Follow Summer Academy as it happens on the SA Blog!</p>
			</a>
-->
			
            <!-- **** Registration Suspended **** -->
<!--
  <div class="box_4_reg">
        <img src="../wp-content/uploads/2016/02/box4-hdr.png">
        <strong>REGISTRATION SUSPENDED</strong>
        <p>All classes are full, registration has been closed. Thank you for your interest in Summer Academy</p>
			</a> -->
			
			<!-- ****Countdown Timer****-->
<!--
			<div class="box_4_ctd">
			<div class="counting">Counting down to:</div>
			<div class="smaca">Summer Academy</div>
			<div class="year">2016</div>
							<span id="future_date"></span>
			<div class="togo">TO GO!</div>
		</div>
-->
		
<!-- *****Blue Registration Starting**** -->
<!--
        <div class="box_4"><img src="../wp-content/themes/nssacademy/images/box4-hdr.png" alt="">
        <p class="holder">Online Registration Starts March 1st</p>
-->

</div>
    </div>
  </div>
  <div class="home_program_block">
    <div class="container">
      <h2><?php echo get_field("heading",44);?></h2>
      <div class="row">
        <div class="col-sm-12 col-md-6">
          <div class="big-txt">
           <?php echo get_field("description",44);?></div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="video_block"> <?php echo get_field("video_link",44);?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_template_part( 'inc/parent', 'testimonial' ); ?>
<?php get_template_part( 'inc/vision', 'logo' ); ?>
<?php get_template_part( 'inc/twitter', 'footer' ); ?>
<?php get_footer();?>
