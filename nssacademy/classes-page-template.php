<?php
/**
 * Template Name: Classes Page
 */
get_header();
?>

<!-- page title section -->
<div class="page_title">
    <div class="container">
        <h1><?php the_title(); ?> </h1>
    </div>
</div>

<!-- bread crumbs -->
<div class="bread_crumbs">
    <div class="container">
        <?php if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('<ul id="breadcrumbs" class="breadcrumb"><li>', '</li></ul>');
        }
        ?>
    </div>
</div>
<div class="main-content">
    <div class="container">
        <div class="information-block"> <img src="<?php echo get_template_directory_uri(); ?>/images/information-img.png" alt="" class="pull-left">
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            wp_reset_postdata();
            ?>
            <span class="close">X</span> </div>
        <div class="filter-block"><strong class="pull-left">Filter by Grade</strong>
            <?php
            $field_key = "field_56c174fd273db";
            $field = get_field_object($field_key);
            ?>
            <form>
                <div>
                    <select class="custom_detail">
                        <option value="all">All Grades</option>
<?php foreach ($field['choices'] as $k => $v) { ?>
                            <option value="<?php echo $k; ?>"><?php echo $v; ?></option>
<?php } ?>
                    </select>
                </div>
            </form>
            <small>(Current grade for 2019-2020).</small></div>
        <div class="row grade_filter">
            <?php
            $args = array(
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'ASC',
                'post_type' => 'class',
                'post_status' => 'publish',
                'suppress_filters' => true);
            $myposts = get_posts($args);
            foreach ($myposts as $post) : setup_postdata($post);
                $feat_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                ?>
                <div class="col-sm-6 col-md-4">
                    <div class="class">
                        <figure><a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/timthumb.php?src=<?php echo $feat_image; ?>&h=230&w=360" alt=""></a></figure>
                        <article> <span class="grade_rate">
                           Class # <?php echo $post->menu_order; ?> &nbsp;
                        <img src="<?php bloginfo('template_url'); ?>/images/grade-icon.png" alt="">Grades: <?php
                           $field_grade = get_post_meta($post->ID, "grdaes", true);
                                $i = 1;
                                foreach ($field_grade as $field_grade_val) {
                                    $count = count($field_grade);
                                    ?>
                                    <?php
                                    if ($i === $count) {
                                        echo str_replace("select_", " ", $field_grade_val);
                                    } else {
                                        echo str_replace("select_", " ", $field_grade_val) . "-";
                                    }
                                    ?>
                            <?php $i++;} ?>
                            </span>
                            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>
                            <a href="<?php the_permalink(); ?>" class="button default">Full details </a> </article>
                    </div>
                </div>
<?php endforeach; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
<script>
    $(document).ready(function (e) {

        $('.custom_detail').change(function () {

            data =
                    {
                        action: 'show_class',
                        'metakey': $(this).val()
                    };

            $.post('<?php echo get_template_directory_uri(); ?>/grade_filter_ajax.php', data, function (result) {

              
                $('.grade_filter').html(result);
                $('.class').removeAttr("style");
                  set_max_height();
            });


        });

    });


</script>